import { badRequest, serverError, success } from '../helpers/http-helper'
import {
  HttpRequest,
  HttpResponse,
  AddProduct,
  Controller,
  Validation
} from './add-product-protocols'

export class AddProductController implements Controller {
  constructor(
    private readonly validation: Validation,
    private readonly addProduct: AddProduct
  ) {}

  async handle(httpRequest: HttpRequest): Promise<HttpResponse> {
    try {
      const isFieldValid = this.validation.validate(httpRequest.body)
      if (isFieldValid) {
        return badRequest(isFieldValid)
      }
      const product = await this.addProduct.add(httpRequest.body)
      return success(product)
    } catch (error) {
      return serverError(error)
    }
  }
}
